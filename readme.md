## 如何在Debian12上安装docker引擎（不是桌面版），使用国内源安装，速度更快

> 环境：Debian12

> 安装方式：apt安装

> 安装目标：docker引擎

> 用户：root

### 第一步，避免干扰，先卸载老版本（如果有的话），命令如下：

```bash
# 老版本的Docker名为： docker, docker.io或者docker-engine
apt remove docker docker.io containerd runc

```

### 第二步，设置仓库源，命令如下：

```bash
# 1.安装https的依赖
apt install apt-transport-https ca-certificates curl gnupg  lsb-release

# 2.添加Docker中科大的 GPG key
curl -fsSL http://mirrors.ustc.edu.cn/docker-ce/linux/debian/gpg | gpg --dearmor -o /etc/apt/keyrings/docker-archive-keyring.gpg

# 3.设置stable源
echo "deb [arch=amd64 signed-by=/etc/apt/keyrings/docker-archive-keyring.gpg] http://mirrors.ustc.edu.cn/docker-ce/linux/debian \
  $(lsb_release -cs) stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null


```

### 第三步，安装docker

```bash
# 1. 更新软件源
apt update

# 2. 安装docker
apt install docker-ce docker-ce-cli containerd.io
```

### 第四步，验证docker是否安装成功

```bash
docker run hello-world

```

### 第五步，设置国内镜像源，加速镜像安装

```bash
# 1. 编辑/etc/docker/daemon.json文件（没有则新建），加入如下内容：

{
    "registry-mirrors": [
        "https://dockerproxy.com",
        "https://hub-mirror.c.163.com",
        "https://mirror.baidubce.com",
        "https://ccr.ccs.tencentyun.com"
    ]
}

# 2. 重启docker服务

systemctl restart docker

# 3. 使用如下命令验证：`docker info`，有显示如下内容则成功：

Registry Mirrors:
  https://dockerproxy.com/
  https://hub-mirror.c.163.com/
  https://mirror.baidubce.com/
  https://ccr.ccs.tencentyun.com/

```
